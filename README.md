# Projeto Bank

Bem-vindo ao projeto Bank, desenvolvido por um grupo de 6 alunos como parte de um trabalho em equipe. Este projeto em Java visa implementar funcionalidades bancárias essenciais, melhorando a experiência do usuário em transações financeiras. Abaixo estão as principais funcionalidades implementadas até o momento.

## Funcionalidades Implementadas

### 1) Transferência por Pix
A capacidade de transferência por meio do Pix oferece aos usuários a possibilidade de realizar transações financeiras de forma instantânea. Essa funcionalidade inovadora utiliza o sistema Pix como meio de facilitar e agilizar a movimentação de fundos entre diferentes contas bancárias. Além de proporcionar uma velocidade excepcional na conclusão das transações, esse método também promove conveniência aos clientes, eliminando a necessidade de aguardar longos períodos para que o dinheiro seja transferido. Com isso, os usuários experimentam maior fluidez e praticidade em suas operações financeiras do dia a dia.


### 2) Pagamento
A funcionalidade de pagamento oferece uma maneira simplificada e ágil para os clientes quitar suas contas e boletos. Através dessa facilidade, é possível realizar pagamentos de forma rápida e segura diretamente pelo aplicativo, eliminando a necessidade de deslocamento físico até agências bancárias ou pontos de atendimento. Ao utilizar essa opção, os clientes ganham conveniência e praticidade, pois conseguem gerenciar suas finanças de maneira eficiente, economizando tempo e simplificando suas transações diárias.


### 3) Investimento
A função de investimento proporciona aos clientes a oportunidade de aplicar seu dinheiro diretamente pelo aplicativo bancário. Essa ferramenta permite que os usuários explorem uma variedade de opções de investimento, capacitando-os a tomar decisões embasadas e estratégicas para fazer seu dinheiro render. Ao oferecer acesso fácil a diferentes modalidades de investimento, os clientes podem diversificar suas carteiras e escolher aquelas que melhor se alinham aos seus objetivos financeiros. Isso não apenas simplifica o processo de investir, mas também capacita os clientes a expandirem seu conhecimento financeiro e a tomarem decisões mais informadas para maximizar o potencial de retorno de seus investimentos.

## Contatos

Em caso de dúvidas ou problemas, entre em contato com a nossa equipe de desenvolvimento:

- Ravar Souza - ravar@example.com
- Erick Afonso - erickafonsocc@gmail.com
- Gabriel Brum - gabiconcli@gmail.com
- Eduardo Corrêa - eduardo@example.com
- Pedro Cruz - pedro@example.com
- Davi Santos - davi@example.com

Agradecemos pela sua contribuição, paciência e interesse no projeto Bank!
